" pathogen settings
call pathogen#infect()
call pathogen#helptags()

" turn off MacVim toolbar
:set guioptions-=T

" removing MacVim scrollbar
:set guioptions-=r

filetype on
filetype plugin on
filetype indent on
set autoindent
set tabstop=2
set shiftwidth=2
set expandtab
set autoindent
set nowrap
set smartindent
set ruler

syntax on
set incsearch
set linespace=2
autocmd FileType python set tabstop=2
autocmd FileType python set shiftwidth=2
autocmd FileType javascript set tabstop=2
autocmd FileType javascript set shiftwidth=2
au BufNewFile,BufRead *.tt2 set filetype=html
set nobackup

" set visual bell -- i hate that damned beeping
set noeb vb t_vb=

" theme settings
syntax enable
set encoding=utf-8 nobomb

" Powerline
let g:Powerline_symbols = 'fancy'

" Airline
let g:airline_powerline_fonts = 1

" UI settings
if has('mac')
  set guifont=Monaco\ for\ Powerline:h14
elseif has('unix')
  set guifont=Ubuntu\ Mono\ for\ Powerline\ 16
endif

if has('gui_running')
  set background=dark
  colorscheme onedark
else
  set background=dark
  colorscheme onedark
endif

set number

" keep some stuff in the history
set history=100

" set the textwidth to be 120 chars
set textwidth=120

" display giode for the testwidth
set colorcolumn=120

" Syntax coloring lines that are too long just slows down the world
set synmaxcol=2048

" toggle the NERD Tree Tabs on an off with Ctrl + N
nmap <C-N> :NERDTreeToggle<CR>

" close the NERD Tree with Shift-F7
nmap <S-F7> :NERDTreeClose<CR>

" locate file in NERD Tree with ff
nmap <F><F> :NERDTreeFind<CR>

" Ctrl-P mapping with space space
let g:ctrlp_map = '<space><space>'

"ruby autocomplete stuff
autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete
autocmd FileType ruby,eruby let g:rubycomplete_buffer_loading = 1
autocmd FileType ruby,eruby let g:rubycomplete_rails = 1
autocmd FileType ruby,eruby let g:rubycomplete_classes_in_global = 1

"other autocomplete stuff
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS

"improve autocomplete menu color
highlight Pmenu ctermbg=238 gui=bold

" Buffer Navigation
map <C-down> :BufExplorer<CR>

" Command+Option+Right for next
map <D-M-Right> :tabn<CR>

" Command+Option+Left for previous
map <D-M-Left> :tabp<CR>

" Speed up the scroll speed in Macvim
:map <S-ScrollWheelUp> <C-U>
:map <S-ScrollWheelDown> <C-D>

" Highlight trailing white spaces on non-insert mode
highlight ExtraWhitespace ctermbg=red guibg=red
au ColorScheme * highlight ExtraWhitespace guibg=red
au BufEnter * match ExtraWhitespace /\s\+$/
au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
au InsertLeave * match ExtraWhiteSpace /\s\+$/

" Nerdtree show hidden files
let NERDTreeShowHidden=1
